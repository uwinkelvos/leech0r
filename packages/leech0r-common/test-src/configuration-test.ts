import Test from "tape";

import { isHostHeader } from "../lib/configuration";

Test("isHostHeader", function(t) {
	t.ok(isHostHeader({}), "empty hosts object is ok!");
	t.ok(isHostHeader({ h1: {} }), "empty headers object is ok!");
	t.ok(isHostHeader({ h1: { r: "string" } }), "one host one header is ok!");
	t.ok(isHostHeader({ h1: {}, h2: {} }), "multiple hosts without headers are ok!");
	t.ok(isHostHeader({ h1: { r: "string" }, h2: { r: "string" } }), "multiple hosts with headers are ok!");
	t.notOk(isHostHeader([]), "array as hosts is not ok!");
	t.notOk(isHostHeader(""), "string as hosts is not ok!");
	t.notOk(isHostHeader(1), "number as hosts is not ok!");
	t.notOk(isHostHeader({ h1: [] }), "array as headers is not ok!");
	t.notOk(isHostHeader({ h1: "no-object" }), "string as headers is not ok!");
	t.notOk(isHostHeader({ h1: 1 }), "number as headers is not ok!");
	t.notOk(isHostHeader({ h1: { r: {} } }), "object as header is not ok!");
	t.notOk(isHostHeader({ h1: { r: [] } }), "array as header is not ok!");
	t.notOk(isHostHeader({ h1: { r: 1 } }), "number as header is not ok!");
	t.notOk(isHostHeader({ h1: {}, h2: "no-object" }), "mixed valid and invalid headers are not ok!");
	t.notOk(isHostHeader({ h1: { r: "string" }, h2: { r: {} } }), "mixed valid and invalid header are not ok!");
	t.end();
});
