export type ItemSetStatus = "STASHED" | "QUEUED" | "FINISHED";

export interface ItemSetData {
	id: string;
	status: ItemSetStatus;
	items: ItemData[];
}

export type ItemStatus = "PENDING" | "ACTIVE" | "SUCCESS" | "ERROR";

export interface ItemData {
	url: string;
	fileName: string | null;
	status: ItemStatus;
	bytesRead: number;
	bytesTotal: number | null;
}
