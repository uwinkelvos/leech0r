export interface HostHeader {
	[host: string]: { [header: string]: string };
}

export function isHostHeader(obj: any): obj is HostHeader {
	return (
		typeof obj === "object" &&
		!Array.isArray(obj) &&
		Object.values(obj).every(
			host =>
				typeof host === "object" &&
				!Array.isArray(host) &&
				Object.values(host).every(header => typeof header === "string")
		)
	);
}
