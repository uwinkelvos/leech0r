# leech0r

a restful file loader

## config

    // ~/.config/leech0r.json
    {
      "concurrency": 2,
      "downloadDir": "__DOWNLOAD_DIR__",
      "hostHeader": {
        "uploaded.net": {"cookie": "login=__LOGIN_COOKIE__"},
        "ul.to": {"cookie": "login=__LOGIN_COOKIE__"}
      }
    }
