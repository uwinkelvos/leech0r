"use strict";

import * as Collections from "./core/Collections";
import * as Runner from "./core/Runner";
import * as Screen from "./screen/SimpleScreen";
import * as Fs from "fs";
import * as Path from "path";
import * as Url from "url";
import * as Util from "./core/Util";

const argv = process.argv.slice(2);
const config = JSON.parse(Fs.readFileSync(Path.join(process.env.HOME, ".config", "leech0r.json"), "UTF-8"));

let itemStore = new Collections.ItemStore();
//console.log(config);
let runner = new Runner.HttpRunner(itemStore, config.concurrency, config.downloadDir, config.hostHeader);

//itemStore.on('item::started', (item:collections.Item)=> {
//	console.log("item::started: " + item.url);
//});
//
//itemStore.on('item::finished', (item:collections.Item)=> {
//	console.log("item::finished: " + item.url);
//});
//
//itemStore.on('itemset::finished', (itemSet:collections.ItemSet)=> {
//	console.log("itemset::finished: " + itemSet.id);
//});

argv.forEach(url => {
	itemStore.add(Util.getFilename(Url.parse(url)), [url], false);
});

runner.start();

let screen = new Screen.SimpleScreen();

let interval = setInterval(() => {
	let lines: string[] = [`status `, ""];
	itemStore.itemSets().forEach(itemSet => {
		lines.push(itemSet.id);
		itemSet.items.forEach(item => {
			lines.push(
				`- ${item.url} -> ${item.path !== null ? item.path : "(not set)"} ${item.calculatePercentage().toFixed()} %`
			);
		});
	});
	screen.render(lines);
	if (itemStore.itemSets(Collections.ItemSetStatus.QUEUED).length === 0) {
		clearInterval(interval);
	}
}, 123);
