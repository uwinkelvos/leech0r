"use strict";

import * as Blessed from "blessed";

export class SimpleScreen {
	private linesWritten: number;
	private tput: Blessed.Tput;

	constructor() {
		this.linesWritten = 0;
		this.tput = Blessed.tput({
			terminal: process.env.TERM,
			extended: true
		});
	}

	private printLine(text: string) {
		process.stdout.write(text + "\n");
		this.linesWritten++;
	}

	render(lines: string[]) {
		//clear screen
		for (let i = 0; i < this.linesWritten; i++) {
			process.stdout.write(this.tput["cuu1"]());
		}
		this.linesWritten = 0;
		process.stdout.write(this.tput["ed"]());
		lines.forEach(line => {
			this.printLine(line);
		});
	}
}
