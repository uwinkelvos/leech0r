declare module "blessed" {
	export interface TputFunc {
		(...args: string[]): string;
	}

	export interface Tput {
		[func: string]: TputFunc;
	}

	export function tput(init: any): Tput;
}
