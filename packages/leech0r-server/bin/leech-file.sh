#!/bin/bash

if [ -z "$1" ] && [ -z "$2" ];
	then echo "usage: $0 [HOST] [FILE]"; exit 1;
fi

TOKEN=$(curl 'http://wundertuete:3000/api/login' -H 'Content-Type: application/json' -d '{"username":" ", "password":" "}')
curl -X POST -H "content-type:application/json" -H "Authorization: Bearer ${TOKEN//\"}" "http://$1/api/itemsets" -d @"$2"
