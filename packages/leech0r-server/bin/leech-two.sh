#!/bin/bash

if [ -z "$1" ] && [ -z "$2" ] && [ -z "$3" ] && [ -z "$4" ];
	then echo "usage: $0 [HOST] [ID] [URL1] [URL2]"; exit 1;
fi

curl -X POST -H "content-type:application/json" "http://$1/api/itemsets" -d "{\"id\":\"$2\", \"urls\": [\"$3\", \"$4\"]}"
