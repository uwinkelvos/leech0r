#!/bin/bash

if [ -z "$1" ] && [ -z "$2" ];
	then echo "usage: $0 [HOST] [URL]"; exit 1;
fi

curl -X POST -H "content-type:application/json" "http://$1/api/itemsets" -d "{\"id\":\"$(basename $2)\", \"urls\": [\"$2\"]}"
