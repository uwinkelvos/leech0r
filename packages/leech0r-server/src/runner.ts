import { ItemStore, ItemSet, Item } from "./collection";

import * as Fs from "fs";
import * as Path from "path";
import * as Http from "http";
import * as Url from "url";

import Debug from "debug";
import * as Request from "request";
import * as ContentDisposition from "content-disposition";

import { HostHeader } from "leech0r-common/lib/configuration";

const dbg = Debug("leech0r:runner");

export abstract class RunnerBase {
	private _itemStore: ItemStore;
	private _maxConcurrent: number;
	private _currentItems: Item[];

	constructor(itemStore: ItemStore, maxConcurrent: number) {
		this._itemStore = itemStore;
		this._maxConcurrent = maxConcurrent;
		this._currentItems = [];
	}

	get maxConcurrent(): number {
		return this._maxConcurrent;
	}
	setMaxConcurrent(maxConcurrent: number) {
		this._maxConcurrent = maxConcurrent;
		this.startNext();
	}

	start(): void {
		this.startNext();
		this._itemStore.on("item::finished", (item: Item) => {
			this._currentItems.splice(this._currentItems.indexOf(item), 1);
			this.startNext();
		});
		this._itemStore.on("itemset::added", (itemSet: ItemSet) => {
			for (const item of itemSet.items) {
				this.initialize(item);
			}
			this.startNext();
		});
	}

	stop(kill: boolean): void {
		this._itemStore.removeAllListeners();
		if (kill) {
			this._currentItems.forEach(item => {
				item.finalize(false);
			});
		}
	}

	private startNext(): void {
		while (this._currentItems.length < this._maxConcurrent) {
			let item = this._itemStore.nextItem();
			if (item !== null) {
				dbg("starting: " + item.url);
				this._currentItems.push(item);
				this.leech(item);
			} else {
				dbg("no next item in queue!");
				break;
			}
		}
		dbg(`runner queue: ${this._currentItems.length}/${this._maxConcurrent}`);
	}

	protected abstract initialize(item: Item): void;
	protected abstract leech(item: Item): void;
}

export class HttpRunner extends RunnerBase {
	private _protocols = /^https?:$/;
	private _downloadDir: string;
	private _hostHeader: HostHeader;

	constructor(itemStore: ItemStore, concurrency: number, downloadDir: string, hostHeader: HostHeader) {
		super(itemStore, concurrency);
		this._downloadDir = downloadDir;
		this._hostHeader = hostHeader;
	}

	get downloadDir(): string {
		return this._downloadDir;
	}
	setDownloadDir(downloadDir: string): void {
		this._downloadDir = downloadDir;
	}

	get hostHeader(): HostHeader {
		return this._hostHeader;
	}
	setHostHeader(hostHeader: HostHeader): void {
		this._hostHeader = hostHeader;
	}

	private getSize(response: Http.IncomingMessage): number {
		return Number(response.headers["content-length"]);
	}

	private getFileName(response: Http.IncomingMessage): string {
		const contentDisposition = response.headers["content-disposition"]
			? ContentDisposition.parse(response.headers["content-disposition"] as string)
			: null;
		return contentDisposition && contentDisposition.parameters["filename"]
			? contentDisposition.parameters["filename"]
			: null;
	}

	protected initialize(item: Item): void {
		dbg("initializing: " + item.url);
		const urlObj = Url.parse(item.url);
		if (this._protocols.test(urlObj.protocol!)) {
			const req = Request.head({
				url: urlObj,
				headers: this._hostHeader[urlObj.hostname!],
				jar: true
			});
			req.on("error", err => {
				dbg(err);
				req.abort();
			});
			req.on("response", (response: Http.IncomingMessage) => {
				dbg(item.url + ": " + response.statusCode);
				if (response.statusCode === 200) {
					const bytesTotal = this.getSize(response);
					const fileName = this.getFileName(response);
					item.initialize(bytesTotal, fileName);
				} else {
					dbg(item.url + ": could not be initialized!");
					req.abort();
				}
			});
		} else {
			dbg(item.url + ": unsupported protocol!");
			item.finalize(false);
		}
	}

	protected leech(item: Item): void {
		dbg("leeching: " + item.url);
		item.start();
		const urlObj = Url.parse(item.url);
		if (this._protocols.test(urlObj.protocol!)) {
			const req = Request.get({
				url: urlObj,
				headers: this._hostHeader[urlObj.hostname!],
				jar: true
			});
			req.on("error", err => {
				dbg(err);
				req.abort();
			});
			req.on("response", (response: Http.IncomingMessage) => {
				dbg(item.url + ": " + response.statusCode);
				if (response.statusCode === 200) {
					const bytesTotal = this.getSize(response);
					const fileName = this.getFileName(response);
					// this can also have happend before in itemset initialization, but the info might have changed in the mean time.
					item.initialize(bytesTotal, fileName ? fileName : Path.parse(urlObj.pathname!).base);

					const filePath = Path.join(this._downloadDir, item.fileName!);
					req.on("data", (data: Buffer) => {
						item.updateBytesRead(data.length);
					});
					req.on("end", () => {
						dbg(item.url + ": end");
						item.finalize(item.bytesRead === item.bytesTotal);
					});
					dbg("writing to: " + filePath);
					try {
						const ostream = Fs.createWriteStream(filePath);
						req.pipe(ostream);
					} catch (err) {
						dbg(err);
						req.abort();
					}
				} else {
					dbg(item.url + ": could not be leeched!");
					item.finalize(false);
					req.abort();
				}
			});
		} else {
			item.finalize(false);
		}
	}
}

export class DummyRunner extends RunnerBase {
	private static TICK_LENGTH = 100;
	private static BYTES_PER_TICK = (1000000 / 1000) * DummyRunner.TICK_LENGTH; // 1MB / s => 1kB/ms => 100kB/tick

	private _fileSizeBase: number;

	constructor(itemStore: ItemStore, fileSizeBase: number) {
		super(itemStore, 2);
		this._fileSizeBase = fileSizeBase;
	}

	protected initialize(item: Item): void {
		item.initialize(Math.round((Math.random() + 0.5) * this._fileSizeBase), "dummy");
	}

	protected leech(item: Item): void {
		dbg("leeching: " + item.url);
		let interval = setInterval(() => {
			let bytesRead =
				item.bytesTotal !== null
					? DummyRunner.BYTES_PER_TICK < item.bytesTotal - item.bytesRead
						? DummyRunner.BYTES_PER_TICK
						: item.bytesTotal - item.bytesRead
					: 0;
			item.updateBytesRead(bytesRead);
			if (item.bytesRead === item.bytesTotal) {
				dbg("item '%s' finished!", item.url);
				item.finalize();
				clearInterval(interval);
			}
		}, DummyRunner.TICK_LENGTH);
	}
}
