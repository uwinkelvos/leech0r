import Debug from "debug";

import { ItemStore } from "./collection";
import { ItemSetStatus } from "leech0r-common/lib/domain";
import { UserCredentials } from "leech0r-common/lib/system";
import { HttpRunner } from "./runner";

import * as Fs from "fs";
import * as Path from "path";
import * as Os from "os";

import Koa from "koa";
import * as Route from "koa-route";
import BodyParser from "koa-bodyparser";
import Cors from "kcors";
import Mount from "koa-mount";
import KoaJwt from "koa-jwt";
import * as Jwt from "jsonwebtoken";
import Send from "koa-send";
import { isHostHeader } from "leech0r-common/lib/configuration";

const dbg = Debug("leech0r:server");

const CONFIG_PATH = Path.join(Os.homedir(), ".config", "leech0r.json");
const CONFIG = JSON.parse(Fs.readFileSync(CONFIG_PATH, "UTF-8"));

const itemStore = new ItemStore();

const api = new Koa();
api.use(Cors());
api.use(BodyParser());

const userStore = {
	authenticate: ({ username, password }: UserCredentials): boolean => {
		return username
			? username
					.split("")
					.reverse()
					.join("") === password
			: false;
	}
};

api.use(
	Route.post("/login", ctx => {
		const credentials = ctx.request.body as UserCredentials;
		if (userStore.authenticate(credentials)) {
			const payload = { sub: credentials.username };
			const token = Jwt.sign(payload, CONFIG.jwtSecret, {
				algorithm: "HS256",
				expiresIn: "1d"
			});
			ctx.body = JSON.stringify(token);
			dbg("login '%s' successfull!", credentials.username);
		} else {
			dbg("login '%s' failed!", credentials.username);
			ctx.throw(401, "authentication failed!");
		}
	})
);

api.use(KoaJwt({ secret: CONFIG.jwtSecret, algorithms: ["HS256"] }));

api.use(
	Route.get("/itemsets", ctx => {
		if (ctx.query.status) {
			const status: ItemSetStatus = ctx.query.status as ItemSetStatus;
			if (status) {
				ctx.body = itemStore.itemSets(status);
			} else {
				throw new Error("unknown status: " + ctx.query.status);
			}
		} else {
			ctx.body = itemStore.itemSets();
		}
	})
);

api.use(
	Route.post("/itemsets", ctx => {
		try {
			const body = ctx.request.body;
			itemStore.add(body.id, body.urls, false);
			ctx.status = 204;
		} catch (ex) {
			ctx.throw(400, ex.message);
		}
	})
);

api.use(
	Route.get("/itemsets/:id", (ctx, id) => {
		ctx.body = itemStore.find(id);
	})
);

api.use(
	Route.put("/itemsets/:id", (ctx, id) => {
		ctx.throw(501, id);
	})
);

api.use(
	Route.del("/itemsets/:id", (ctx, id) => {
		itemStore.remove(id);
		ctx.status = 204;
	})
);

api.use(
	Route.get("/active", ctx => {
		ctx.body = itemStore.active;
	})
);

const runner = new HttpRunner(itemStore, CONFIG.concurrency, CONFIG.downloadDir, CONFIG.hostHeader);
runner.start();

api.use(
	Route.get("/config/hostHeader", ctx => {
		ctx.body = runner.hostHeader;
	})
);

api.use(
	Route.put("/config/hostHeader", ctx => {
		const body = ctx.request.body;
		if (isHostHeader(body)) {
			runner.setHostHeader(body);
			ctx.status = 204;
		} else {
			ctx.throw(400, body);
		}
	})
);

const server = new Koa();
server.use(Mount("/api", api));

server.use(
	Route.get("/favicon.ico", ctx => {
		return Send(ctx, Path.join("node_modules", "leech0r-web", "web", "favicon.ico"), {
			root: Path.join(__dirname, "..")
		});
	})
);
server.use(
	Route.get("/bundle.js", ctx => {
		return Send(ctx, Path.join("node_modules", "leech0r-web", "lib", "bundle.js"), {
			root: Path.join(__dirname, "..")
		});
	})
);
server.use(
	Route.get("*", ctx => {
		return Send(ctx, Path.join("node_modules", "leech0r-web", "web", "index.html"), {
			root: Path.join(__dirname, "..")
		});
	})
);

server.listen(3000, () => {
	dbg("http://0.0.0.0:3000/");
});
