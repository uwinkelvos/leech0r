import * as Events from "events";
import * as Assert from "assert";
import Debug from "debug";

import { ItemSetData, ItemSetStatus, ItemData, ItemStatus } from "leech0r-common/lib/domain";

const dbg = Debug("leech0r:collections");

export class ItemStore extends Events.EventEmitter {
	private _itemSets: ItemSet[];

	constructor() {
		super();
		this._itemSets = [];
	}

	find(id: string): ItemSet | null {
		const item = this._itemSets.find(itemSet => itemSet.id === id);
		return item ? item : null;
	}

	add(id: string, urls: string[], stashed: boolean): void {
		if (!id) {
			throw new Error("id must not be null or empty!");
		} else if (this.find(id)) {
			throw new Error(`Duplicate ItemSet with id '${id}'!`);
		} else if (urls.length === 0) {
			throw new Error("urls must not be empty!");
		} else {
			const items = urls.map(url => {
				return new Item(url);
			});
			const itemSet: ItemSet = new ItemSet(id, items, stashed);
			this._itemSets.push(itemSet);
			this.addItemSetEventListeners(itemSet);
		}
	}

	remove(id: string): void {
		let itemSet = this.find(id);
		if (!itemSet) {
			throw new Error(`ItemSet with id '${id}' could not be found!`);
		}
		this.clearItemSetEventListeners(itemSet);
		this._itemSets.splice(this._itemSets.indexOf(itemSet), 1);
	}

	itemSets(status?: ItemSetStatus): ItemSet[] {
		if (status) {
			return this._itemSets.filter(itemSet => itemSet.status === status);
		} else {
			return this._itemSets;
		}
	}

	get active(): ItemSet[] {
		return this._itemSets.filter(itemSet => itemSet.items.some(item => item.status === "ACTIVE"));
	}

	nextItem(): Item | null {
		for (const itemSet of this.itemSets("QUEUED")) {
			for (const item of itemSet.items) {
				if (item.status === "PENDING") {
					dbg("next item: '%s'!", item.url);
					return item;
				}
			}
		}
		dbg("no next item found!");
		return null;
	}

	private addItemSetEventListeners(itemSet: ItemSet) {
		dbg("addItemSetEventListeners on ItemSet '%s'.", itemSet.id);
		itemSet.addItemEventListeners();
		itemSet.on("item::started", (item: Item) => {
			this.emit("item::started", item);
		});
		itemSet.on("item::finished", (item: Item) => {
			this.emit("item::finished", item);
		});
		itemSet.on("itemset::finished", (itemSet: ItemSet) => {
			this.emit("itemset::finished", itemSet);
			this.clearItemSetEventListeners(itemSet);
		});
		this.emit("itemset::added", itemSet);
	}

	private clearItemSetEventListeners(itemSet: ItemSet) {
		itemSet.removeAllListeners();
		itemSet.clearItemEventListeners();
		this.emit("itemset::removed", itemSet);
	}
}

export class ItemSet extends Events.EventEmitter {
	private _id: string;
	private _status: ItemSetStatus;
	private _items: Item[];

	constructor(id: string, items: Item[], stashed: boolean) {
		super();
		this._id = id;
		this._status = stashed ? "STASHED" : "QUEUED";
		this._items = items;
	}

	get id(): string {
		return this._id;
	}

	get items(): Item[] {
		return this._items;
	}

	get status(): ItemSetStatus {
		return this._status;
	}

	addItemEventListeners() {
		this._items.forEach((item: Item) => {
			dbg("addItemEventListeners on Item '%s'.", item.url);
			item.on("item::started", (item: Item) => {
				this.emit("item::started", item);
			});
			item.on("item::finished", (item: Item) => {
				dbg("Item '%s' in ItemSet '%s' finished with status '%s'!", item.url, this._id, item.status);
				this.emit("item::finished", item);
				let done = this._items.every(item => item.status === "SUCCESS");
				if (done) {
					dbg("ItemSet '%s' finished!", this._id);
					this._status = "FINISHED";
					this.emit("itemset::finished", this);
				}
			});
		});
	}

	clearItemEventListeners(): void {
		this._items.forEach((item: Item) => {
			item.removeAllListeners();
		});
	}

	toJSON(): ItemSetData {
		return {
			id: this.id,
			status: this.status,
			items: this.items.map(item => item.toJSON())
		};
	}
}

export class Item extends Events.EventEmitter {
	private _url: string;
	private _fileName: string | null = null;
	private _bytesTotal: number | null = null;
	private _status: ItemStatus = "PENDING";
	private _bytesRead: number = 0;

	constructor(url: string) {
		super();
		this._url = url;
	}

	get url(): string {
		return this._url;
	}

	get fileName(): string | null {
		return this._fileName;
	}

	get bytesTotal(): number | null {
		return this._bytesTotal;
	}

	get status(): ItemStatus {
		return this._status;
	}

	get bytesRead(): number {
		return this._bytesRead;
	}

	updateBytesRead(newBytes: number): void {
		// TODO: investigate performance, as this is by far the most commonly called method of the application, so this assert could hurt.
		// Assert.ok(this._bytesRead + newBytes <= this._bytesTotal, "bytesRead + newBytes > bytesTotal");
		this._bytesRead += newBytes;
	}

	initialize(bytesTotal: number, fileName: string): void {
		this._bytesTotal = bytesTotal;
		this._fileName = fileName;
	}

	start(): void {
		Assert.ok(this._status === "PENDING");
		Assert.strictEqual(this._bytesRead, 0);
		this._status = "ACTIVE";
		this.emit("item::started", this);
	}

	finalize(success: boolean = true): void {
		if (success) {
			dbg("item '%s' finished successfully.", this._url);
			this._status = "SUCCESS";
		} else {
			dbg("item '%s' finished with error.", this._url);
			this._status = "ERROR";
		}
		this.emit("item::finished", this);
		this.removeAllListeners();
	}

	calculatePercentage(): number {
		switch (this._status) {
			case "PENDING":
				return 0;
			case "ACTIVE":
				return this._bytesTotal ? (this._bytesRead / this._bytesTotal) * 100 : NaN;
			case "SUCCESS":
				return 100;
			case "ERROR":
				return NaN;
		}
	}

	toJSON(): ItemData {
		return {
			url: this.url,
			fileName: this.fileName,
			status: this.status,
			bytesRead: this.bytesRead,
			bytesTotal: this.bytesTotal
		};
	}
}
