import Test from "tape";

import { Item } from "../lib/collection";

Test("Item", function(t) {
	const item = new Item("http://host/");
	t.deepEquals(item.toJSON(), {
		url: "http://host/",
		fileName: null,
		status: "PENDING",
		bytesRead: 0,
		bytesTotal: null
	});
	t.end();
});
