import Test from "tape";

import { userReducer } from "../lib/user/state";

Test("User", function(t) {
	t.deepEquals(userReducer(null, { type: "USER_SET", token: "token", name: "name" }), {
		token: "token",
		name: "name"
	});
	t.end();
});
