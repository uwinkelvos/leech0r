const Path = require("path");
const Webpack = require("webpack");

module.exports = {
	entry: Path.join(__dirname, "lib", "index.js"),
	output: {
		path: Path.join(__dirname, "lib"),
		filename: "bundle.js"
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				enforce: "pre",
				loader: "source-map-loader"
			}
		]
	},
	plugins: [
		new Webpack.HotModuleReplacementPlugin(),
		new Webpack.EnvironmentPlugin({
			NODE_ENV: "development",
			BACKEND_URL: "/api/"
		})
	],
	devtool: "source-map",
	devServer: {
		hot: true,
		inline: true,
		contentBase: "web",
		host: "0.0.0.0",
		historyApiFallback: true
	}
};
