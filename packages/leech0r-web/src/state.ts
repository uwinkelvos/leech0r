import { itemSetsReducer, ItemSetsState } from "./itemsets/state";
import { errorsReducer, ErrorsState } from "./errors/state";
import { userReducer, UserState } from "./user/state";
import { BackendProvider } from "./backend";
import { configReducer, ConfigState } from "./config/state";

import { default as thunkMiddleware } from "redux-thunk";
import { RouterState, connectRouter, routerMiddleware } from "connected-react-router";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, Store, createStore, combineReducers, applyMiddleware } from "redux";
import { History } from "history";
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction";

export type Leech0rThunkDispatch = ThunkDispatch<Leech0rAppState, BackendProvider, AnyAction>;

export type Leech0rStore = Store<Leech0rAppState, AnyAction> & {
	dispatch: Leech0rThunkDispatch;
};

export interface Leech0rAppState {
	itemSets: ItemSetsState;
	errors: ErrorsState;
	user: UserState;
	config: ConfigState;
	router: RouterState;
}

export function createLeech0rStore(backendProvider: BackendProvider, history: History): Leech0rStore {
	return createStore(
		combineReducers<Leech0rAppState>({
			itemSets: itemSetsReducer,
			errors: errorsReducer,
			user: userReducer,
			config: configReducer,
			router: connectRouter(history)
		}),
		composeWithDevTools(
			applyMiddleware(thunkMiddleware.withExtraArgument(backendProvider), routerMiddleware(history))
		)
	);
}
