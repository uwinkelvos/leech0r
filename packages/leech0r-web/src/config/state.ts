import { Action, Reducer, combineReducers } from "redux";
import { ThunkAction } from "redux-thunk";
import { HostHeader } from "leech0r-common/lib/configuration";
import { Leech0rAppState } from "../state";
import { BackendProvider } from "../backend";
import { IErrorsAdd } from "../errors/state";

type HostHeaderState = HostHeader;

interface IHostHeaderSet extends Action {
	type: "CONFIG_HOSTHEADER_SET";
	value: HostHeader;
}

const hostHeaderReducer: Reducer<HostHeaderState, IHostHeaderSet> = (state = {}, action) => {
	switch (action.type) {
		case "CONFIG_HOSTHEADER_SET":
			return action.value;
		default:
			return state;
	}
};

export function hostHeaderFetch(): ThunkAction<
	Promise<HostHeader>,
	Leech0rAppState,
	BackendProvider,
	IHostHeaderSet | IErrorsAdd
> {
	return (dispatch, _, getBackend) => {
		// TODO: dispatch spinner action
		return getBackend()
			.hostHeaderFetch()
			.then(
				hostHeader => {
					dispatch({
						type: "CONFIG_HOSTHEADER_SET",
						value: hostHeader
					});
					return hostHeader;
				},
				error => {
					dispatch({
						type: "ERRORS_ADD",
						error: error.message
					});
					return Promise.reject(error);
				}
			);
	};
}

export function hostHeaderSet(
	value: HostHeader
): ThunkAction<Promise<void>, Leech0rAppState, BackendProvider, IHostHeaderSet | IErrorsAdd> {
	return (dispatch, _, getBackend) => {
		// TODO: dispatch spinner action
		return getBackend()
			.hostHeaderSet(value)
			.then(
				() => {
					dispatch({
						type: "CONFIG_HOSTHEADER_SET",
						value
					});
				},
				error => {
					dispatch({
						type: "ERRORS_ADD",
						error: error.message
					});
				}
			);
	};
}

export type ConfigAction = IHostHeaderSet;

export interface ConfigState {
	hostHeader: HostHeaderState;
}

export const configReducer = combineReducers<ConfigState, ConfigAction>({
	hostHeader: hostHeaderReducer
});
