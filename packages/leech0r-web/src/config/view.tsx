import React from "react";
import { connect } from "react-redux";
import { Leech0rAppState, Leech0rThunkDispatch } from "../state";
import { TextField, Button } from "@material-ui/core";
import { hostHeaderSet } from "./state";
import { HostHeader, isHostHeader } from "leech0r-common/lib/configuration";

export const ConfigRoutePath = "/config";

interface JsonPanelDispatchProps<T> {
	onSave: (obj: T) => void;
}

interface JsonPanelStateProps<T> {
	obj: T;
}

type JsonPanelProps<T> = JsonPanelDispatchProps<T> & JsonPanelStateProps<T>;

interface JsonPanelState {
	json: string;
	error: boolean;
}

abstract class JsonPanel<T> extends React.Component<JsonPanelProps<T>, JsonPanelState> {
	constructor(props: JsonPanelProps<T>) {
		super(props);
		this.state = {
			json: this.formatJson(props.obj),
			error: false
		};
	}

	render(): JSX.Element {
		return (
			<form>
				<TextField
					id="host-header"
					label="HostHeader"
					value={this.state.json}
					placeholder="HostHeader as JSON"
					multiline
					margin="normal"
					variant="outlined"
					fullWidth
					error={this.state.error}
					onChange={ev => {
						const json = ev.target.value;
						try {
							this.parseJson(json);
							this.setState({ error: false });
						} catch (error) {
							this.setState({ error: true });
						} finally {
							this.setState({ json });
						}
					}}
					onBlur={() => {
						if (!this.state.error) {
							this.setState(prevState => ({
								json: this.formatJson(JSON.parse(prevState.json))
							}));
						}
					}}
				/>
				<div style={{ display: "flex", justifyContent: "flex-end" }}>
					<Button
						color="primary"
						onClick={() => {
							this.setState({
								json: this.formatJson(this.props.obj),
								error: false
							});
						}}
					>
						Reset
					</Button>
					<Button
						variant="contained"
						color="primary"
						disabled={this.state.error}
						onClick={() => {
							this.props.onSave(this.parseJson(this.state.json));
						}}
					>
						Save
					</Button>
				</div>
			</form>
		);
	}

	protected formatJson(obj: T): string {
		return JSON.stringify(obj, null, 3);
	}

	protected parseJson(json: string): T {
		const obj = JSON.parse(json);
		if (this.validate(obj)) {
			return obj;
		} else {
			throw new Error("obj is not valid!");
		}
	}

	protected abstract validate(obj: any): obj is T;
}

// could we pass this fuctionality in via props within connect?
class JsonPanelForHostHeader extends JsonPanel<HostHeader> {
	protected validate(obj: any): obj is HostHeader {
		return isHostHeader(obj);
	}
}

const HostHeaderPanel = connect<
	JsonPanelStateProps<HostHeader>,
	JsonPanelDispatchProps<HostHeader>,
	{},
	Leech0rAppState
>(
	(state: Leech0rAppState) => {
		return {
			obj: state.config.hostHeader
		};
	},
	(dispatch: Leech0rThunkDispatch) => ({
		onSave: (obj: object) => {
			dispatch(hostHeaderSet(obj as HostHeader));
		}
	})
)(JsonPanelForHostHeader);

export function ConfigRoute(_props: object): JSX.Element {
	return (
		<div>
			<HostHeaderPanel />
		</div>
	);
}
