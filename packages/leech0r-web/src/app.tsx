import * as React from "react";

import { Route } from "react-router";

import { MuiThemeProvider, createMuiTheme, CssBaseline } from "@material-ui/core";
import { green, grey, red } from "@material-ui/core/colors";

import { ItemSetRoute, ItemSetRoutePath } from "./itemsets/view";
import { UserLoginRoute, UserLoginRoutePath } from "./user/login";
import { AppErrors } from "./errors/view";
import { AppFrame } from "./frame/view";
import { ConfigRoute, ConfigRoutePath } from "./config/view";

const muiTheme = createMuiTheme({
	palette: {
		primary: {
			main: green[600]
		},
		secondary: grey,
		error: red
	},
	typography: {
		useNextVariants: true
	}
});

export function Leech0rApp(): JSX.Element {
	return (
		<MuiThemeProvider theme={muiTheme}>
			<CssBaseline />
			<div>
				<AppErrors />
				<AppFrame />
				<div
					style={{
						minWidth: "300px",
						width: "80%",
						margin: "0 auto 0 auto"
					}}
				>
					<Route path={ItemSetRoutePath} component={ItemSetRoute} />
					<Route path={UserLoginRoutePath} component={UserLoginRoute} />
					<Route path={ConfigRoutePath} component={ConfigRoute} />
				</div>
			</div>
		</MuiThemeProvider>
	);
}

export default Leech0rApp;
