import { Action, Reducer } from "redux";

export type ErrorsState = string[];

export interface IErrorsAdd extends Action {
	type: "ERRORS_ADD";
	error: string;
}

export interface IErrorsClear extends Action {
	type: "ERRORS_CLEAR";
	index: number;
}

export const errorsReducer: Reducer<ErrorsState, IErrorsAdd | IErrorsClear> = (
	state: ErrorsState = [],
	action: IErrorsAdd | IErrorsClear
): ErrorsState => {
	switch (action.type) {
		case "ERRORS_ADD": {
			// TODO: configurable?
			if (state.length < 3) {
				return [...state, action.error];
			} else {
				console.log(action.error);
				return state;
			}
		}
		case "ERRORS_CLEAR": {
			return [...state.slice(0, action.index), ...state.slice(action.index + 1)];
		}
		default: {
			return state;
		}
	}
};
