import React from "react";

import { connect } from "react-redux";

import { Leech0rAppState } from "../state";

import { IErrorsClear } from "./state";
import { Button, Dialog, DialogActions, DialogTitle } from "@material-ui/core";

export interface ErrorsStateProps {
	errors: string[];
}
export interface ErrorsDispatchProps {
	onClose: (index: number) => void;
}

function Errors(props: ErrorsStateProps & ErrorsDispatchProps) {
	return (
		<div>
			{...props.errors.map((error, idx) => {
				return (
					<Dialog
						key={idx}
						open={true}
						onClose={() => {
							props.onClose(idx);
						}}
					>
						<DialogTitle>{error}</DialogTitle>
						<DialogActions>
							<Button
								color="primary"
								onClick={() => {
									props.onClose(idx);
								}}
							>
								Close
							</Button>
						</DialogActions>
					</Dialog>
				);
			})}
		</div>
	);
}

export const AppErrors = connect<ErrorsStateProps, ErrorsDispatchProps, {}, Leech0rAppState>(
	(state: Leech0rAppState) => ({ errors: state.errors }),
	dispatch => ({
		onClose: idx => {
			dispatch<IErrorsClear>({ type: "ERRORS_CLEAR", index: idx });
		}
	})
)(Errors);
