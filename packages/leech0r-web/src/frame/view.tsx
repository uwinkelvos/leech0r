import React from "react";
import { connect } from "react-redux";

import {
	Button,
	AppBar,
	Typography,
	IconButton,
	withStyles,
	createStyles,
	WithStyles,
	Toolbar,
	Drawer,
	List,
	ListItem,
	ListItemIcon,
	ListItemText
} from "@material-ui/core";
import { Menu as MenuIcon, Settings as SettingsIcon, List as ListIcon } from "@material-ui/icons";

import { push as historyPush } from "connected-react-router";

import { Leech0rAppState, Leech0rThunkDispatch } from "../state";

import { userLogout } from "../user/state";
import { UserLoginRoutePath } from "../user/login";
import { ItemSetRoutePath } from "../itemsets/view";
import { ConfigRoutePath } from "../config/view";

const styles = createStyles({
	root: {
		flexGrow: 1
	},
	grow: {
		flexGrow: 1
	},
	menuButton: {
		marginLeft: -12,
		marginRight: 20
	}
});

export interface AppFrameStateProps {
	user: string | null;
}

export interface AppFrameDispatchProps {
	onLogin(): void;
	onLogout(): void;
	onNavigation(link: string): void;
}

type AppFrameProps = AppFrameStateProps & AppFrameDispatchProps & WithStyles<typeof styles>;

interface AppFrameState {
	showNav: boolean;
}

class Leech0rAppBar extends React.Component<AppFrameProps, AppFrameState> {
	constructor(props: AppFrameProps) {
		super(props);
		this.state = { showNav: false };
	}

	public render(): JSX.Element {
		return (
			<div className={this.props.classes.root}>
				<AppBar position="static">
					<Toolbar>
						<IconButton
							className={this.props.classes.menuButton}
							color="inherit"
							aria-label="Menu"
							onClick={() => {
								this.toggleNav();
							}}
						>
							<MenuIcon />
						</IconButton>
						<Typography variant="h6" color="inherit" className={this.props.classes.grow}>
							Leech0rApp
						</Typography>
						{this.props.user ? (
							<Button
								color="inherit"
								onClick={() => {
									this.props.onLogout();
								}}
							>{`logout (${this.props.user})`}</Button>
						) : (
							<Button
								color="inherit"
								onClick={() => {
									this.props.onLogin();
								}}
							>
								login
							</Button>
						)}
					</Toolbar>
				</AppBar>
				<Drawer
					open={this.state.showNav}
					onClose={() => {
						this.toggleNav();
					}}
				>
					<div
						tabIndex={0}
						role="button"
						onClick={() => {
							this.toggleNav();
						}}
						onKeyDown={() => {
							this.toggleNav();
						}}
					>
						<List>
							<ListItem
								button
								key={ItemSetRoutePath}
								onClick={() => {
									this.props.onNavigation(ItemSetRoutePath);
								}}
							>
								<ListItemIcon>
									<ListIcon />
								</ListItemIcon>
								<ListItemText primary="ItemSets" />
							</ListItem>
							<ListItem
								button
								key={ConfigRoutePath}
								onClick={() => {
									this.props.onNavigation(ConfigRoutePath);
								}}
							>
								<ListItemIcon>
									<SettingsIcon />
								</ListItemIcon>
								<ListItemText primary="Configuration" />
							</ListItem>
						</List>
					</div>
				</Drawer>
			</div>
		);
	}

	private toggleNav(): void {
		this.setState(prevState => ({ showNav: !prevState.showNav }));
	}
}

export const AppFrame = connect<AppFrameStateProps, AppFrameDispatchProps, {}, Leech0rAppState>(
	(state: Leech0rAppState) => ({ user: state.user ? state.user.name : null }),
	(dispatch: Leech0rThunkDispatch) => ({
		onLogin: () => {
			dispatch(historyPush(UserLoginRoutePath));
		},
		onLogout: () => {
			dispatch(userLogout());
		},
		onNavigation: (link: string) => {
			dispatch(historyPush(link));
		}
	})
)(withStyles(styles)(Leech0rAppBar));
