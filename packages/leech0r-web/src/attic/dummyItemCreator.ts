import { ItemSetData, ItemSetStatus, ItemData, ItemStatus } from "leech0r-common/lib/domain";

export default function createItemSet(id: string, urls: string[]): ItemSetData {
	const status = ["STASHED", "QUEUED", "FINISHED"][Math.round(Math.random() * 2)] as ItemSetStatus;
	const itemDatas: ItemData[] = urls.map(url => createItem(url, status));
	return {
		id: id,
		status: status,
		items: itemDatas
	};
}

function createItem(url: string, setStaus: ItemSetStatus): ItemData {
	switch (setStaus) {
		case "STASHED":
			return {
				url: url,
				fileName: null,
				status: "PENDING",
				bytesRead: 0,
				bytesTotal: null
			};
		case "QUEUED": {
			const status = ["PENDING", "ACTIVE", "SUCCESS", "ERROR"][Math.round(Math.random() * 3)] as ItemStatus;

			return {
				url: url,
				fileName: status === "ACTIVE" ? "path/to/file.ext" : null,
				status: status,
				bytesRead: status !== "PENDING" ? Math.random() * 100 : 0,
				bytesTotal: status !== "PENDING" ? 100 : null
			};
		}
		case "FINISHED": {
			const status = ["SUCCESS", "ERROR"][Math.round(Math.random() * 1)] as ItemStatus;
			return {
				url: url,
				fileName: "path/to/file.ext",
				status: status,
				bytesRead: Math.random() * 100,
				bytesTotal: 100
			};
		}
		default:
			throw new Error("unknown status: " + status);
	}
}
