import { Action, Reducer } from "redux";
import { ThunkAction } from "redux-thunk";

import { ItemSetData, ItemData } from "leech0r-common/lib/domain";

import { Leech0rAppState } from "../state";
import { BackendProvider } from "../backend";
import { IErrorsAdd } from "../errors/state";

export type ItemSetsState = ItemSetData[];

export interface IItemSetsAdd extends Action {
	type: "ITEMSETS_ADD";
	id: string;
	urls: string[];
}

export interface IItemSetsFetch extends Action {
	type: "ITEMSETS_FETCH";
	itemSets: ItemSetData[];
}

export const itemSetsReducer: Reducer<ItemSetsState, IItemSetsAdd | IItemSetsFetch> = (
	state: ItemSetsState = [],
	action: IItemSetsAdd | IItemSetsFetch
): ItemSetsState => {
	switch (action.type) {
		case "ITEMSETS_FETCH": {
			return [...action.itemSets];
		}
		case "ITEMSETS_ADD": {
			const items: ItemData[] = action.urls.map(url => {
				const ItemData: ItemData = {
					url: url,
					fileName: null,
					status: "PENDING",
					bytesRead: 0,
					bytesTotal: null
				};
				return ItemData;
			});
			const itemSet: ItemSetData = {
				id: action.id,
				status: "QUEUED",
				items: items
			};
			return [...state, itemSet];
		}
		default: {
			return state;
		}
	}
};

export function itemSetsFetch(): ThunkAction<
	Promise<ItemSetData[]>,
	Leech0rAppState,
	BackendProvider,
	IItemSetsFetch | IErrorsAdd
> {
	return (dispatch, _, getBackend) => {
		// TODO: dispatch spinner action
		return getBackend()
			.itemSetsFetch()
			.then(
				itemSets => {
					dispatch({
						type: "ITEMSETS_FETCH",
						itemSets: itemSets
					});
					return itemSets;
				},
				error => {
					dispatch({
						type: "ERRORS_ADD",
						error: error.message
					});
					return Promise.reject(error);
				}
			);
	};
}

export function itemSetsAdd(
	id: string,
	urls: string[]
): ThunkAction<Promise<void>, Leech0rAppState, BackendProvider, IItemSetsAdd | IErrorsAdd> {
	return (dispatch, _, getBackend) => {
		// TODO: dispatch spinner action
		return getBackend()
			.itemSetsAdd(id, urls)
			.then(
				() => {
					dispatch({
						type: "ITEMSETS_ADD",
						id,
						urls
					});
				},
				error => {
					dispatch({
						type: "ERRORS_ADD",
						error: error.message
					});
				}
			);
	};
}
