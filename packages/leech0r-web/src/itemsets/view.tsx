import React from "react";
import { connect } from "react-redux";

import { Route, Switch } from "react-router";
import { push as historyPush } from "connected-react-router";

import { Leech0rAppState, Leech0rThunkDispatch } from "../state";

import { ItemSetList, ItemSetListInitialProps, ItemSetListDispatchProps } from "./list";
import { ItemSetPanel, ItemSetPanelDispatchProps } from "./add";
import { itemSetsAdd } from "./state";

export const ItemSetRoutePath = "/";
const ItemSetRouteNewPath = ItemSetRoutePath + "new";

const VisibleItemSetList = connect<ItemSetListInitialProps, ItemSetListDispatchProps, {}, Leech0rAppState>(
	(state: Leech0rAppState) => ({ itemSets: state.itemSets }),
	dispatch => ({
		onAdd: () => {
			dispatch(historyPush(ItemSetRouteNewPath));
		}
	})
)(ItemSetList);

const AddItemSet = connect<{}, ItemSetPanelDispatchProps, {}, Leech0rAppState>(
	null,
	(dispatch: Leech0rThunkDispatch) => ({
		onSubmit: (id, urls) => {
			dispatch(itemSetsAdd(id, urls));
		},
		onClose: () => {
			dispatch(historyPush(ItemSetRoutePath));
		}
	})
)(ItemSetPanel);

export function ItemSetRoute(): JSX.Element {
	return (
		<div>
			<Switch>
				<Route exact strict path={ItemSetRoutePath} render={() => <VisibleItemSetList />} />
				<Route exact strict path={ItemSetRouteNewPath} render={() => <AddItemSet />} />
			</Switch>
		</div>
	);
}
