import React from "react";
import { ItemSetData, ItemSetStatus, ItemData, ItemStatus } from "leech0r-common/lib/domain";

import {
	Fab,
	List,
	ListItem,
	LinearProgress,
	ListItemIcon,
	ListItemText,
	ListItemSecondaryAction,
	Collapse,
	Typography
} from "@material-ui/core";

import { Add, CloudDone, CloudDownload, CloudOff, CloudQueue, ExpandMore, ExpandLess } from "@material-ui/icons";
const styles = {
	floatingActionButton: {
		position: "fixed",
		bottom: 32,
		right: 32
	},
	iconButton: {
		top: 16
	}
};

export interface ItemSetListDispatchProps {
	onAdd(): void;
}

export interface ItemSetListInitialProps {
	itemSets: ItemSetData[];
}

export type ItemSetListProps = ItemSetListInitialProps & ItemSetListDispatchProps;

export interface ItemSetListState {
	itemSetItemsVisible: Map<string, boolean>;
}

export class ItemSetList extends React.Component<ItemSetListProps, ItemSetListState> {
	constructor(props: ItemSetListProps) {
		super(props);
		this.state = {
			itemSetItemsVisible: new Map(props.itemSets.map<[string, boolean]>(is => [is.id, false]))
		};
	}

	// // TODO: -> getDerivedStateFromProps
	// static getDerivedStateFromProps(props: ItemSetListProps, state: ItemSetListState): Partial<ItemSetListState> | null {
	// 	return {
	// 		itemSetItemsVisible: new Map(props.itemSets.map<[string, boolean]>(is => [is.id, state.itemSetItemsVisible.get(is.id) || false]))
	// 	};
	// }
	componentWillReceiveProps(nextProps: ItemSetListProps): void {
		this.setState({
			itemSetItemsVisible: new Map(
				nextProps.itemSets.map<[string, boolean]>(is => [is.id, this.state.itemSetItemsVisible.get(is.id) || false])
			)
		});
	}

	render(): JSX.Element {
		return (
			<div>
				<List>{...this.props.itemSets.map(itemset => this.renderItemSet(itemset))}</List>
				<Fab
					color="primary"
					style={{
						margin: 0,
						top: "auto",
						right: 20,
						bottom: 20,
						left: "auto",
						position: "fixed",
						zIndex: 100
					}}
					onClick={() => {
						this.props.onAdd();
					}}
				>
					<Add />
				</Fab>
			</div>
		);
	}

	private handleItemSetClick(id: string): void {
		this.setState(state => {
			state.itemSetItemsVisible.set(id, !state.itemSetItemsVisible.get(id));
			return {
				itemSetItemsVisible: state.itemSetItemsVisible
			};
		});
	}

	private renderItemSet(itemset: ItemSetData): JSX.Element {
		const { bytesRead, bytesTotal } = itemset.items.reduce(
			(prv, cur) => ({
				bytesRead: prv.bytesRead + cur.bytesRead,
				bytesTotal: prv.bytesTotal + (cur.bytesTotal !== null ? cur.bytesTotal : 0)
			}),
			{ bytesRead: 0, bytesTotal: 0 }
		);

		return (
			<div key={itemset.id}>
				<ListItem
					button
					onClick={() => {
						this.handleItemSetClick(itemset.id);
					}}
				>
					<ListItemIcon>
						{this.renderItemSetStatus(itemset.status, itemset.items.map(itm => itm.status))}
					</ListItemIcon>
					<ListItemText
						disableTypography
						primary={<Typography>{itemset.id}</Typography>}
						secondary={
							<LinearProgress variant="determinate" value={bytesTotal ? (bytesRead / bytesTotal) * 100 : 0} />
						}
					/>
					<ListItemSecondaryAction>
						{
							// TODO:
							// <Menu style={styles.iconButton} iconButtonElement={
							// 	<IconButton touch={true} tooltip="actions" tooltipPosition="bottom-left">
							// 		<MoreVert />
							// 	</IconButton>
							// }>
							// 	<MenuItem>delete </MenuItem>
							// </Menu>
						}
					</ListItemSecondaryAction>
					{this.state.itemSetItemsVisible.get(itemset.id) ? <ExpandLess /> : <ExpandMore />}
				</ListItem>
				<Collapse in={this.state.itemSetItemsVisible.get(itemset.id)}>
					<List style={{ paddingLeft: 20 }}>
						{itemset.items.map((item, index) => this.renderItem(item, index))}
					</List>
				</Collapse>
			</div>
		);
	}

	private renderItemSetStatus(status: ItemSetStatus, itemStati: ItemStatus[]): JSX.Element {
		switch (status) {
			case "STASHED":
				return <CloudQueue style={styles.iconButton} />;
			case "QUEUED":
				if (itemStati.some(itemStatus => itemStatus === "ERROR")) {
					return <CloudOff style={styles.iconButton} />;
				} else {
					return <CloudDownload style={styles.iconButton} />;
				}
			case "FINISHED":
				if (itemStati.every(itemStatus => itemStatus === "SUCCESS")) {
					return <CloudDone style={styles.iconButton} />;
				} else {
					return <CloudOff style={styles.iconButton} />;
				}
			default:
				throw new Error("unknown status: " + status);
		}
	}

	private renderItem(item: ItemData, index: number): JSX.Element {
		const bytesRead = item.bytesTotal ? item.bytesRead : 0;
		const bytesTotal = item.bytesTotal ? item.bytesTotal : 1;
		return (
			<ListItem key={index}>
				<ListItemIcon>{this.renderItemStatus(item.status)}</ListItemIcon>
				<ListItemText
					disableTypography
					primary={<Typography>{item.url}</Typography>}
					secondary={<LinearProgress variant="determinate" value={(bytesRead / bytesTotal) * 100} />}
				/>
				<ListItemSecondaryAction>
					{
						// TODO:
						// <Menu iconButtonElement={
						// 	<IconButton style={styles.iconButton} tooltip="actions" tooltipPosition="bottom-left" >
						// 		<MoreVert />
						// 	</IconButton >
						// }>
						// 	<MenuItem>restart</MenuItem>
						// </Menu >
					}
				</ListItemSecondaryAction>
			</ListItem>
		);
	}

	private renderItemStatus(status: ItemStatus): JSX.Element {
		switch (status) {
			case "PENDING":
				return <CloudQueue style={styles.iconButton} />;
			case "ACTIVE":
				return <CloudDownload style={styles.iconButton} />;
			case "SUCCESS":
				return <CloudDone style={styles.iconButton} />;
			case "ERROR":
				return <CloudOff style={styles.iconButton} />;
			default:
				throw new Error("unknown status: " + status);
		}
	}
}

export default ItemSetList;
