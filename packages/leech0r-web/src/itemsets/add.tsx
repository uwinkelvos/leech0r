import React from "react";

import { Button, TextField, IconButton } from "@material-ui/core";
import { Delete } from "@material-ui/icons";

export interface ItemSetPanelInitialProps {
	id?: string;
	urls?: string[];
}

export interface ItemSetPanelDispatchProps {
	onClose: () => void;
	onSubmit: (id: string, urls: string[]) => void;
}

export interface ItemSetPanelState {
	id: string;
	urls: string[];
	newUrl: string;
}

export type ItemSetPanelProps = ItemSetPanelInitialProps & ItemSetPanelDispatchProps;

export class ItemSetPanel extends React.Component<ItemSetPanelProps, ItemSetPanelState> {
	constructor(props: ItemSetPanelProps) {
		super(props);
		this.state = {
			id: props.id || "",
			urls: props.urls || [],
			newUrl: ""
		};
	}

	public render(): JSX.Element {
		const urlElements: JSX.Element[] = this.state.urls.map((url, index) => {
			return (
				<div key={index} style={{ display: "flex" }}>
					<TextField label="URL" helperText="URL" value={url} fullWidth style={{ flex: 1 }} disabled />
					<IconButton
						onClick={() => {
							this.onDeleteUrl(index);
						}}
						style={{ flex: 0 }}
					>
						<Delete />
					</IconButton>
				</div>
			);
		});
		return (
			<form>
				<TextField
					label="ID"
					helperText="ID"
					fullWidth
					onChange={ev => {
						this.onChangeId(ev.target.value);
					}}
					value={this.state.id}
				/>
				{...urlElements}
				<TextField
					label="URL"
					helperText="URL"
					fullWidth
					onChange={ev => {
						this.onChangeUrl(ev.target.value);
					}}
					onBlur={() => {
						this.onBlurUrl();
					}}
					value={this.state.newUrl}
				/>
				<div style={{ display: "flex", justifyContent: "flex-end" }}>
					<Button
						color="primary"
						style={{}}
						onClick={() => {
							this.onCancel();
						}}
					>
						Cancel
					</Button>
					<Button
						variant="contained"
						color="primary"
						style={{}}
						onClick={() => {
							this.onSubmit();
						}}
					>
						Submit
					</Button>
				</div>
			</form>
		);
	}

	protected onCancel(): void {
		this.props.onClose();
	}

	protected onSubmit(): void {
		this.props.onSubmit(this.state.id, this.state.urls);
		this.props.onClose();
	}

	protected onChangeId(id: string): void {
		this.setState({ id: id });
	}

	protected onDeleteUrl(index: number): void {
		this.setState(prevState => ({
			urls: [...prevState.urls.slice(0, index), ...prevState.urls.slice(index + 1)]
		}));
	}

	protected onChangeUrl(url: string): void {
		this.setState({ newUrl: url });
	}

	protected onBlurUrl(): void {
		const newUrls = this.state.newUrl.split(/\s/).filter(u => u);
		if (newUrls.length > 0) {
			this.setState(prevState => ({
				urls: [...prevState.urls, ...newUrls],
				newUrl: ""
			}));
		}
	}
}

export default ItemSetPanel;
