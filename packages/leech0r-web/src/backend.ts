import { ItemSetData } from "leech0r-common/lib/domain";
import { UserCredentials } from "leech0r-common/lib/system";
import { HostHeader } from "leech0r-common/lib/configuration";

import { Leech0rStore } from "./state";
import { itemSetsFetch } from "./itemsets/state";

export interface BackendProvider {
	(): Backend;
}

export abstract class Backend {
	private _pollLoopTimer: number | null = null;
	constructor(protected readonly _store: Leech0rStore) {
		this._store.subscribe(() => {
			// TODO: We need some backendpoll state management.
			if (this._pollLoopTimer === null && this._store.getState().user !== null) {
				this._pollLoopTimer = setInterval(() => {
					this._store.dispatch(itemSetsFetch());
					this._store.dispatch(hostHeaderFetch());
				}, 1000);
			} else if (this._pollLoopTimer !== null && this._store.getState().user === null) {
				clearInterval(this._pollLoopTimer);
				this._pollLoopTimer = null;
			}
		});
	}
	destroy() {
		if (this._pollLoopTimer) {
			clearInterval(this._pollLoopTimer);
		}
	}
	abstract itemSetsFetch(): Promise<ItemSetData[]>;
	abstract itemSetsAdd(id: string, urls: string[]): Promise<void>;
	abstract userLogin(credentials: UserCredentials): Promise<string>;
	abstract hostHeaderFetch(): Promise<HostHeader>;
	abstract hostHeaderSet(hostHeader: HostHeader): Promise<void>;
}

import dummyItemSetCreator from "./attic/dummyItemCreator";
import { hostHeaderFetch } from "./config/state";

export class TimerBackend extends Backend {
	private _itemSets = [dummyItemSetCreator("id1", ["url11", "url12"]), dummyItemSetCreator("id2", ["url21", "url22"])];
	private _hostHeader = {};

	constructor(store: Leech0rStore, protected readonly _errorRate: number = 0) {
		super(store);
	}

	itemSetsFetch(): Promise<ItemSetData[]> {
		const fail: boolean = Math.random() < this._errorRate;
		if (!fail) {
			return new Promise(resolve => {
				setTimeout(() => {
					this._itemSets.forEach(itemSet => {
						itemSet.items.forEach(item => {
							item.bytesRead *= 1 + (item.bytesTotal! - item.bytesRead) / item.bytesTotal! / 10;
						});
					});
					resolve(this._itemSets);
				}, 100);
			});
		} else {
			return new Promise((_, reject) => {
				setTimeout(() => {
					reject(new Error("random error!"));
				}, 150);
			});
		}
	}
	itemSetsAdd(id: string, urls: string[]): Promise<void> {
		const fail: boolean = Math.random() < this._errorRate;
		if (!fail) {
			return new Promise<void>(resolve => {
				setTimeout(() => {
					this._itemSets.push(dummyItemSetCreator(id, urls));
					resolve();
				}, 100);
			});
		} else {
			return new Promise<void>((_, reject) => {
				setTimeout(() => {
					reject(new Error("random error!"));
				}, 150);
			});
		}
	}
	userLogin({ username, password }: UserCredentials): Promise<string> {
		if (
			username &&
			username
				.split("")
				.reverse()
				.join("") === password
		) {
			const payload = {
				sub: username,
				iat: Date.now(),
				exp: Math.floor(Date.now() / 1000 + 10 * 60)
			};
			const encoded = btoa(JSON.stringify(payload))
				.replace(/\+/g, "-")
				.replace(/\//g, "_");
			return Promise.resolve(`.${encoded}.`);
		} else {
			return Promise.reject(new Error("inavlid credentials!"));
		}
	}
	hostHeaderFetch(): Promise<HostHeader> {
		return Promise.resolve(this._hostHeader);
	}
	hostHeaderSet(hostHeader: HostHeader): Promise<void> {
		this._hostHeader = hostHeader;
		return Promise.resolve();
	}
}

export class FetchBackend extends Backend {
	constructor(store: Leech0rStore, protected readonly _baseUrl: string) {
		super(store);
	}

	private getJwtToken(): string {
		const user = this._store.getState().user;
		const token = user ? user.token : null;
		if (token) {
			return token;
		} else {
			throw new Error("unauthorized!");
		}
	}

	itemSetsFetch(): Promise<ItemSetData[]> {
		return fetch(this._baseUrl + "itemsets", {
			headers: { Authorization: "Bearer " + this.getJwtToken() }
		}).then<ItemSetData[]>(response => {
			if (response.ok) {
				return response.json();
			} else {
				return Promise.reject(new Error(response.statusText));
			}
		});
	}
	itemSetsAdd(id: string, urls: string[]): Promise<void> {
		// TODO: return ItemSetData?
		return fetch(this._baseUrl + "itemsets", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + this.getJwtToken()
			},
			body: JSON.stringify({ id, urls })
		}).then<void>(response => {
			if (response.ok) {
				return Promise.resolve();
			} else {
				return Promise.reject(new Error(response.statusText));
			}
		});
	}
	userLogin(credentials: UserCredentials): Promise<string> {
		return fetch(this._baseUrl + "login", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(credentials)
		}).then<string>(response => {
			if (response.ok) {
				return response.json();
			} else {
				return Promise.reject(new Error(response.statusText));
			}
		});
	}
	hostHeaderFetch(): Promise<HostHeader> {
		return fetch(this._baseUrl + "config/hostHeader", {
			headers: { Authorization: "Bearer " + this.getJwtToken() }
		}).then<HostHeader>(response => {
			if (response.ok) {
				return response.json();
			} else {
				return Promise.reject(new Error(response.statusText));
			}
		});
	}
	hostHeaderSet(hostHeader: HostHeader): Promise<void> {
		return fetch(this._baseUrl + "config/hostHeader", {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + this.getJwtToken()
			},
			body: JSON.stringify(hostHeader)
		}).then<void>(response => {
			if (response.ok) {
				return Promise.resolve();
			} else {
				return Promise.reject(new Error(response.statusText));
			}
		});
	}
}
