import React from "react";
import { render } from "react-dom";

import jwtDecode from "jwt-decode";

import { Provider } from "react-redux";

import createHistory from "history/createBrowserHistory";
import { ConnectedRouter } from "connected-react-router";

import { Leech0rStore, createLeech0rStore } from "./state";
import { IUserSet } from "./user/state";

import { FetchBackend, TimerBackend, Backend } from "./backend";

import { Leech0rApp } from "./app";

declare var process: { env: { [varname: string]: any } };
const CONFIG = {
	NODE_ENV: process.env.NODE_ENV as string,
	BACKEND_URL: process.env.BACKEND_URL as string
};

let backend: Backend;
const history = createHistory();
const store: Leech0rStore = createLeech0rStore(() => backend, history);

backend = CONFIG.NODE_ENV !== "development" ? new FetchBackend(store, CONFIG.BACKEND_URL) : new TimerBackend(store);

const token = window.localStorage.getItem("userToken");
if (token) {
	const payload = jwtDecode<{ sub: string; iat: number; exp: number }>(token);
	const currentSeconds = Date.now() / 1000;
	if (currentSeconds < payload.exp) {
		store.dispatch<IUserSet>({
			type: "USER_SET",
			token,
			name: payload.sub
		});
	}
}

document.addEventListener("DOMContentLoaded", () => {
	render(
		React.createElement(
			Provider,
			{ store },
			React.createElement(ConnectedRouter, { history }, React.createElement(Leech0rApp))
		),
		document.querySelector("#root")
	);
});
