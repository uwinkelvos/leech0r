import React from "react";

import { connect } from "react-redux";

import { push as historyPush } from "connected-react-router";

import { Button, TextField } from "@material-ui/core";

import { UserCredentials } from "leech0r-common/lib/system";

import { userLogin } from "./state";
import { Leech0rAppState, Leech0rThunkDispatch } from "../state";

export const UserLoginRoutePath = "/login";

interface UserLoginPanelDispatchProps {
	onLogin: (credentials: UserCredentials, remember: boolean) => void;
	onClose: () => void;
}

interface UserLoginPanelState {
	username: string;
	password: string;
	remember: boolean;
}

type UserLoginPanelProps = UserLoginPanelDispatchProps;

class UserLoginPanel extends React.Component<UserLoginPanelProps, UserLoginPanelState> {
	constructor(props: UserLoginPanelProps) {
		super(props);
		this.state = {
			username: "",
			password: "",
			remember: true
		};
	}

	public render(): JSX.Element {
		// TODO: make remember state switchable
		return (
			<form>
				<TextField
					label="username"
					helperText="username"
					onChange={ev => {
						this.setState({ username: ev.target.value });
					}}
					fullWidth={true}
				/>
				<TextField
					label="password"
					helperText="password"
					onChange={ev => {
						this.setState({ password: ev.target.value });
					}}
					fullWidth={true}
				/>
				<div style={{ display: "flex", justifyContent: "flex-end" }}>
					<Button
						color="primary"
						style={{}}
						onClick={() => {
							this.onCancel();
						}}
					>
						Cancel
					</Button>
					<Button
						variant="contained"
						color="primary"
						style={{}}
						onClick={() => {
							this.onLogin();
						}}
					>
						Login
					</Button>
				</div>
			</form>
		);
	}

	protected onCancel(): void {
		this.props.onClose();
	}

	protected onLogin(): void {
		this.props.onLogin({ username: this.state.username, password: this.state.password }, this.state.remember);
		this.props.onClose();
	}
}

const UserLogin = connect<{}, UserLoginPanelDispatchProps, {}, Leech0rAppState>(
	null,
	(dispatch: Leech0rThunkDispatch) => ({
		onLogin: (credentials: UserCredentials, remember: boolean) => {
			dispatch(userLogin(credentials, remember));
		},
		onClose: () => {
			// TODO: return to previous route!
			dispatch(historyPush("/"));
		}
	})
)(UserLoginPanel);

export function UserLoginRoute(): JSX.Element {
	return <UserLogin />;
}
