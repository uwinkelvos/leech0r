import { Action, Reducer } from "redux";
import { ThunkAction } from "redux-thunk";

import { UserCredentials } from "leech0r-common/lib/system";

import { Leech0rAppState } from "../state";
import { BackendProvider } from "../backend";
import { IErrorsAdd } from "../errors/state";

export type UserState = null | {
	token: string;
	name: string;
};

export interface IUserSet extends Action {
	type: "USER_SET";
	token: string;
	name: string;
}

export interface IUserUnset extends Action {
	type: "USER_UNSET";
}

export const userReducer: Reducer<UserState> = (state: UserState = null, action: IUserSet | IUserUnset): UserState => {
	switch (action.type) {
		case "USER_SET": {
			return { token: action.token, name: action.name };
		}
		case "USER_UNSET": {
			return null;
		}
		default: {
			return state;
		}
	}
};

export function userLogin(
	credentials: UserCredentials,
	remember: boolean
): ThunkAction<Promise<string>, Leech0rAppState, BackendProvider, IUserSet | IErrorsAdd> {
	return (dispatch, getState, getBackend) => {
		if (!getState().user) {
			// TODO: dispatch spinner action
			return getBackend()
				.userLogin(credentials)
				.then(
					token => {
						dispatch<IUserSet>({
							type: "USER_SET",
							token: token,
							name: credentials.username
						});
						if (remember) {
							window.localStorage.setItem("userToken", token);
						}
						return token;
					},
					error => {
						dispatch<IErrorsAdd>({
							type: "ERRORS_ADD",
							error: error.message
						});
						return Promise.reject(error);
					}
				);
		} else {
			const error = "user already logged in!";
			dispatch<IErrorsAdd>({
				type: "ERRORS_ADD",
				error: error
			});
			return Promise.reject(new Error(error));
		}
	};
}

export function userLogout(): ThunkAction<Promise<void>, Leech0rAppState, BackendProvider, IUserUnset | IErrorsAdd> {
	return (dispatch, getState, _) => {
		if (getState().user) {
			dispatch({ type: "USER_UNSET" });
			if (window.localStorage.getItem("userToken")) {
				window.localStorage.removeItem("userToken");
			}
			return Promise.resolve();
		} else {
			const error = "no user logged in!";
			dispatch({
				type: "ERRORS_ADD",
				error: error
			});
			return Promise.reject(new Error(error));
		}
	};
}
